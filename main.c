#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
/*Поиск указанной строки в указанном файле. Обработка одной строки в порожденном процессе.
Параметры командной строки: 
1.файл, в котором ищем
2.Строка, которую ищем*/
int findstr(char *file_name, char *find_str){
	FILE*file;
 	int pos = 0;
	char string[200], ch, arg[200];
	int count_str = 0, status, stat;

	if((file = fopen(file_name, "r")) == NULL){
		printf("Don't open file for reading");
		exit(-2);
	}
	//считаем количество строк в файле
	while((ch = fgetc(file)) != EOF){
		if(ch == '\n') count_str++;
	}
	printf("Count string in file: %d\n", count_str);
	rewind(file);//указатель на начало файла

	pid_t pid[count_str];
	for (int i = 0; i < count_str; i++) {
		strcpy(arg, find_str);
        // запускаем дочерний процесс 
        pid[i] = fork();

        fgets(string,sizeof(string),file);
        pos = strlen(string) - 1;
        if(string[pos] == '\n') string[pos] = '\0';

        if (-1 == pid[i]) {
            perror("fork"); //произошла ошибка
            exit(-1); //выход из родительского процесса
        } 
        else if (0 == pid[i]) {            
            printf(" CHILD: it's %d process start!\n", i);
            if(execl("./file","file", arg, string, NULL) < 0){
            	printf("Error with start process\n");
            	exit(-2);
            }
        }
    }
 	// если выполняется родительский процесс
    printf("It's PARENT!\n");
    // ожидание окончания выполнения всех запущенных процессов
    for (int i = 0; i < count_str; i++) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("process-child %d done,  result=%d\n", i, WEXITSTATUS(stat));
        }
    }
    printf("Result:\n0 - process-child found the string\n1 - process-child did not find the string\n");
    fclose(file);
    return 0;
	}

int main(int argc, char *argv[]){
	if(argc < 3){
		printf("Need more arguments!\n");
		exit(-1);
	}
	findstr(argv[1], argv[2]);
}