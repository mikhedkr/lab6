#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
 int main(int argc, char * argv[])
 {
 	if(argc < 3){
		printf("Need more arguments!\n");
		exit(-1);
	}
	if(strcmp(argv[1],argv[2]) == 0){
    	return 0;
    }
 	return 1;
 }